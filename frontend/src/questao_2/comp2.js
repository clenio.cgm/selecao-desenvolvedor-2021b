import { useContext } from "react";
import ContextProva from "./context/context";

function Comp2() {
    const { texto, setTexto } = useContext(ContextProva)
    return (
      <div>
        <input value={texto} onChange={event => setTexto(event.target.value)} />
      </div>
    );
  }
  
export default Comp2;
