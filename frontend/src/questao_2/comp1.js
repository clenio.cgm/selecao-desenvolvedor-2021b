import { useContext } from "react";
import ContextProva from "./context/context";

function Comp1() {
  const { texto } = useContext(ContextProva);
  return (
    <div>
      {texto}
    </div>
  );
}
  
export default Comp1;
