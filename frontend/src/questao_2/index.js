import Comp1 from './comp1'
import Comp2 from './comp2'
import ProviderProva from './context/provider';

function Questao2() {
  return (
    <ProviderProva>
      <div>
        <h1>Questão 2</h1>
        <Comp1 />
        <Comp2 />
      </div>
    </ProviderProva>
  );
}
  
export default Questao2;
