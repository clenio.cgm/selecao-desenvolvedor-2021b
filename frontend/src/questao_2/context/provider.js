import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ContextProva from './context';

function ProviderProva({ children }) {
  const textoInit = 'Texto';

  const [texto, setTexto] = useState(textoInit);

  return (
    <ContextProva.Provider
      value={ { texto, setTexto } }
    >
      {children}
    </ContextProva.Provider>
  );
}

ProviderProva.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ProviderProva;
