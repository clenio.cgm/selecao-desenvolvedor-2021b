import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';

// solução encontrada e adaptada do site: https://pt-br.reactjs.org/docs/portals.html

const body = document.querySelector('body');

function Portal({ children }) {
  const el = document.createElement('div');

  useEffect(() => {
    body.appendChild(el);
  }, [el]);

  return ReactDOM.createPortal(
    children,
    el
  );
}

function compRodape() {
  return (
    <Portal>
      Rodapé
    </Portal>
  );
}

export default compRodape;
