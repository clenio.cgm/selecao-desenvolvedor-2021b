import { useState, useEffect } from "react";

function Questao3() {
  const [valor, setValor] = useState('');

  useEffect(() => {
    setValor(sessionStorage.getItem('texto'));
  }, []);

  return (
    <div>
      <h1>Questão 3</h1>
      <input value={valor} onChange={(event) => {
        setValor(event.target.value);
        sessionStorage.setItem('texto', event.target.value);
      }} />
    </div>
  );
}
  
export default Questao3;
