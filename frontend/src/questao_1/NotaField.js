import React, { useState } from 'react'
import { FaStar } from 'react-icons/fa';

function NotaField({ maxNota, value, onChange }) {
  const [ hover, setHover ] = useState(null)

  function onClickValue (event) {
      const nota = event.target.value

      if (nota <= maxNota) {
          onChange(nota)
      }
  }

  return (
    <div>
      {[ ...Array(maxNota)].map((star, index) => {
        const starValue = index + 1;
        return (
          <label key={index}>
            <input
              display="none"
              type="radio"
              name="nota"
              value={starValue} onClick={onClickValue}/>
            <FaStar
              color={starValue <= (hover || value) ? '#FF0000' : '#808080'}
              onMouseEnter={() => setHover(starValue)}
              onMouseLeave={() => setHover(null)}
            />
          </label>
        )
      })}
    </div>
  );
}
  
export default NotaField;
